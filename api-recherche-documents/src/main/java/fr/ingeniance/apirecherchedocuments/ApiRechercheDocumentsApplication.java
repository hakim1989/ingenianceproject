package fr.ingeniance.apirecherchedocuments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRechercheDocumentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRechercheDocumentsApplication.class, args);
	}
}
