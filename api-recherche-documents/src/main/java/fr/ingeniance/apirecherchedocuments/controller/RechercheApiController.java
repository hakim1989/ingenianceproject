/**
 * 
 */
package fr.ingeniance.apirecherchedocuments.controller;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.ingeniance.generatorinterfaceswagger.RechercheDocumentApi;
import fr.ingeniance.generatorinterfaceswagger.ressources.RechercheReponse;

/**
 * @author Consultant LAB
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class RechercheApiController implements RechercheDocumentApi {

	@Override
	public ResponseEntity<RechercheReponse> rechercherDocument(@NotNull @Valid String nomDocument,
			@NotNull @Valid String categorie, @NotNull @Valid String type) {
		HttpStatus status = HttpStatus.OK;

		RechercheReponse reponse = new RechercheReponse();
		reponse.setCategorieDocument("PDF_5");
		reponse.setIdDocument(2L);
		reponse.setNomDocument("api-recherche-document.pdf");
		reponse.setPageDocument(new ArrayList<>());
		reponse.getPageDocument().add("page1");

		return new ResponseEntity<>(reponse, status);
	}

}
