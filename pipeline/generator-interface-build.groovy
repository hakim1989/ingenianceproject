pipeline {
    options {
        buildDiscarder(logRotator(numToKeepStr: '1'))
    }
    
    agent any
	
	tools {
        maven 'maven'
    }

    stages {
        stage('Clean and Package') {
            steps {
                echo "Clean and Package genrator interfaces.."
                dir('generator-interface-swagger') {
					configFileProvider([configFile(fileId: '957e9c64-97e7-4191-8e63-1565a1e8bdcd', variable: 'MAVEN_SETTINGS')]) {
                        sh 'mvn -s \$MAVEN_SETTINGS clean package'
					}						
                }
                
            }
        }

        stage('Deploy') {
            steps {
                echo 'Deploy...'
                dir('generator-interface-swagger') {
					configFileProvider([configFile(fileId: '957e9c64-97e7-4191-8e63-1565a1e8bdcd', variable: 'MAVEN_SETTINGS')]) {
						sh 'mvn -s \$MAVEN_SETTINGS deploy'
						}
                }
                
            }
        }
    }
}
    
