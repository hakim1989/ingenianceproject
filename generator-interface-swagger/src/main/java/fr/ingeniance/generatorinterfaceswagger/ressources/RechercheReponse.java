package fr.ingeniance.generatorinterfaceswagger.ressources;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * RechercheReponse
 */
@Validated

public class RechercheReponse   {
  @JsonProperty("idDocument")
  private Long idDocument = null;

  @JsonProperty("categorieDocument")
  private String categorieDocument = null;

  @JsonProperty("nomDocument")
  private String nomDocument = null;

  @JsonProperty("pageDocument")
  @Valid
  private List<String> pageDocument = null;

  public RechercheReponse idDocument(Long idDocument) {
    this.idDocument = idDocument;
    return this;
  }

  /**
   * Get idDocument
   * @return idDocument
  **/
  @ApiModelProperty(value = "")


  public Long getIdDocument() {
    return idDocument;
  }

  public void setIdDocument(Long idDocument) {
    this.idDocument = idDocument;
  }

  public RechercheReponse categorieDocument(String categorieDocument) {
    this.categorieDocument = categorieDocument;
    return this;
  }

  /**
   * Get categorieDocument
   * @return categorieDocument
  **/
  @ApiModelProperty(value = "")


  public String getCategorieDocument() {
    return categorieDocument;
  }

  public void setCategorieDocument(String categorieDocument) {
    this.categorieDocument = categorieDocument;
  }

  public RechercheReponse nomDocument(String nomDocument) {
    this.nomDocument = nomDocument;
    return this;
  }

  /**
   * Get nomDocument
   * @return nomDocument
  **/
  @ApiModelProperty(value = "")


  public String getNomDocument() {
    return nomDocument;
  }

  public void setNomDocument(String nomDocument) {
    this.nomDocument = nomDocument;
  }

  public RechercheReponse pageDocument(List<String> pageDocument) {
    this.pageDocument = pageDocument;
    return this;
  }

  public RechercheReponse addPageDocumentItem(String pageDocumentItem) {
    if (this.pageDocument == null) {
      this.pageDocument = new ArrayList<String>();
    }
    this.pageDocument.add(pageDocumentItem);
    return this;
  }

  /**
   * Get pageDocument
   * @return pageDocument
  **/
  @ApiModelProperty(value = "")


  public List<String> getPageDocument() {
    return pageDocument;
  }

  public void setPageDocument(List<String> pageDocument) {
    this.pageDocument = pageDocument;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RechercheReponse rechercheReponse = (RechercheReponse) o;
    return Objects.equals(this.idDocument, rechercheReponse.idDocument) &&
        Objects.equals(this.categorieDocument, rechercheReponse.categorieDocument) &&
        Objects.equals(this.nomDocument, rechercheReponse.nomDocument) &&
        Objects.equals(this.pageDocument, rechercheReponse.pageDocument);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idDocument, categorieDocument, nomDocument, pageDocument);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RechercheReponse {\n");
    
    sb.append("    idDocument: ").append(toIndentedString(idDocument)).append("\n");
    sb.append("    categorieDocument: ").append(toIndentedString(categorieDocument)).append("\n");
    sb.append("    nomDocument: ").append(toIndentedString(nomDocument)).append("\n");
    sb.append("    pageDocument: ").append(toIndentedString(pageDocument)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

